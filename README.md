#        >>>>>>> Blockchain Homework003 <<<<<<<

# first clear container

    . ./stop-remove-containers.sh

# Generate material and Up network

    . ./run.sh

# Execute into cli

    docker exec -it cli bash

# Create, join and update channel on peer devPeer1

    . ./scripts/devPeer1.sh

# Join channel on peer accountPeer1

    . ./scripts/accountPeer1.sh

# Join channel on peer hrPeer1

    . ./scripts/hrPeer1.sh

# Join channel on peer marketingPeer1

    . ./scripts/marketingPeer1.sh

# ##### Install Chaincode #######

# Exit to devPeer1 to install chaincode

    export CORE_PEER_MSPCONFIGPATH=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/developers.workspace/users/Admin@developers.workspace/msp
    export CORE_PEER_ADDRESS=peer1.developers.workspace:7051
    export CORE_PEER_LOCALMSPID="Org1MSP"
    export CORE_PEER_TLS_ROOTCERT_FILE=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/developers.workspace/peers/peer1.developers.workspace/tls/ca.crt

# Package chaincode 

    peer lifecycle chaincode package mycc.tar.gz --path ./chaincode --lang golang --label mycc_1

# Install chaincode 

    peer lifecycle chaincode install mycc.tar.gz

# Query installed chaincode 

    peer lifecycle chaincode queryinstalled

# Export chaincode package ID with id from queryed

    export CC_PACKAGE_ID= qureyed id

# Approve chaincode 

    peer lifecycle chaincode approveformyorg --channelID workspace --name mycc --version 1.0 --init-required --package-id $CC_PACKAGE_ID --sequence 1 \
    --tls \
  --cafile /opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/ordererOrganizations/workspace/orderers/orderer1.workspace/msp/tlscacerts/tlsca.workspace-cert.pem

# Check approved chaincode

    peer lifecycle chaincode checkcommitreadiness --channelID workspace --name mycc --version 1.0 --sequence 1 --output json --init-required

# Commit chaincode 

    peer lifecycle chaincode commit -o orderer1.workspace:7050 --channelID workspace --name mycc --version 1.0 --sequence 1 --init-required --peerAddresses peer1.developers.workspace:7051 \
    --tls \
  --cafile /opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/ordererOrganizations/workspace/orderers/orderer1.workspace/msp/tlscacerts/tlsca.workspace-cert.pem \
    --tlsRootCertFiles /opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/developers.workspace/peers/peer1.developers.workspace/tls/ca.crt

# ##### Up explorer #######

# Open new terminal to make it easy

# Go to explorer folder

    cd explorer

# Up explorer

    docker-compose up -d

# ##### Up Grafana #######

# Open new terminal to make it easy

# Go to explorer monitering folder

    cd test-network/moniter and logging/monitering

# Up Grafana

    docker-compose up -d


# ##### Reference #######

# Explorer
    
    ip : http://18.217.231.51:8080/
    username : exploreradmin
    password : exploreradminpw

# Grafana
    
    ip : http://18.217.231.51:3000/
    username : admingrafana
    password : admingrafanapw